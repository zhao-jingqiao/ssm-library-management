<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/3/23
  Time: 14:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>书籍展示</title>
    <%--    BootStrap美化界面--%>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>

<div class="container">
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th>书籍编号</th>
                        <th>书籍名称</th>
                        <th>书籍数量</th>
                        <th>书籍详情</th>
                    </tr>
                    </thead>
                    <%--从数据库查询书籍，在list中遍历出来 foreach--%>
                    <tbody>

                    <tr>
                        <td>${list.bookID}</td>
                        <td>${list.bookName}</td>
                        <td>${list.bookCounts}</td>
                        <td>${list.detail}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

package com.zhao.controller;

import com.zhao.pojo.Books;
import com.zhao.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {
    //调用service层
    @Autowired
    @Qualifier("BookServiceImpl")
    private BookService bookService;
    //查询全部书籍并且返回到书籍展示页面
    @RequestMapping("/allBook")
    public String list(Model model){
        List<Books> books = bookService.queryBook();
        model.addAttribute("list",books);
        return "allBook";
    }
    //跳转到增加书籍页面
    @RequestMapping("/toAddBook")
    public String toAddPaper(){
        return "addBook";
    }
    @RequestMapping("/AddBook")
    public String addBook(Books books){
        System.out.println("addBook-->" +books);
        bookService.addBook(books);
        return "redirect:/book/allBook";
    }
    //跳转到修改页面
    @RequestMapping("/toUpdate")
    public String toUpdatePaper(int id,Model model){
        Books books = bookService.queryBookById(id);
        model.addAttribute("QBook",books);
        return "updateBook";
    }
    //修改书籍
    @RequestMapping("/Update")
    public String updateBook(Books books){
        System.out.println("updateBook-->"+books);
        bookService.updateBook(books);
        return "redirect:/book/allBook";

    }
    @RequestMapping("/toDelete/{bookId}")
    public String toUpdatePaper(@PathVariable("bookId") int id){
        bookService.deleteBookById(id);
        return "redirect:/book/allBook";
    }
    //查询
    @RequestMapping("/queryBook")
    public String queryBook(String queryBookName,Model model){
        Books book = bookService.queryBookByName(queryBookName);
        model.addAttribute("list",book);
        return "queryBookByName";
    }
}
